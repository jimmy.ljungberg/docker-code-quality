# SonarQube via Docker
Exempel för att köra SonarQube via Docker.

## Förutsättningar
  * https://docs.sonarqube.org/latest/requirements/requirements/#header-6

## Installation
  1. Klona detta projekt
  1. skapa en `.env` med följande konfiguration:
     ```
     HTTP_PORT=9000
     ```
  1. Starta SonarQube:
     ```
     docker-compose up -d
     ```
  1. Som standard heter administratörsanvändaren `admin` och har tilldeltas lösenordet `admin`. Logga in och byt lösenord.

## Problem med denna konfiguration
Konfiguration saknas för produktionsmiljö där det behöver sättas upp en separat datas. 
